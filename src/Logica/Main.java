package Logica;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Libro> libros = new ArrayList<>();
        List<Cliente> clientes = new ArrayList<>();
        List<Venta> ventas = new ArrayList<>();

        libros.add(new Libro("El señor de los anillos", "J. R. R. Tolkien", 1954, 10, true));
        libros.add(new Libro("El hobbit", "J. R. R. Tolkien", 1937, 10, true));

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Sistema de gestión de tienda de libros");
            System.out.println("1. Ver libros disponibles");
            System.out.println("2. Registrar un cliente nuevo");
            System.out.println("3. Realizar una compra");
            System.out.println("4. Salir");

            int opcion = scanner.nextInt();
            scanner.nextLine();
            switch (opcion) {
                case 1 -> {
                    System.out.println("Libros disponibles:");
                    for (Libro libro : libros) {
                        System.out.println(libro);
                    }
                }
                case 2 -> {
                    System.out.println("Ingrese el nombre del cliente:");
                    String nombre = scanner.nextLine();
                    System.out.println("Ingrese el apellido del cliente:");
                    String apellido = scanner.nextLine();
                    System.out.println("Ingrese la dirección del cliente:");
                    String direccion = scanner.nextLine();
                    Cliente nuevoCliente = new Cliente(nombre, apellido, direccion);
                    clientes.add(nuevoCliente);
                    System.out.println("Cliente registrado exitosamente.");
                }
                case 3 -> {
                    System.out.println("Ingrese el nombre del cliente:");
                    String clienteNombre = scanner.nextLine();
                    Cliente clienteSeleccionado = null;
                    for (Cliente cliente : clientes) {
                        if (cliente.getNombre().equals(clienteNombre)) {
                            clienteSeleccionado = cliente;
                            break;
                        }
                    }
                    if (clienteSeleccionado == null) {
                        System.out.println("Cliente no encontrado.");
                        break;
                    }
                    System.out.println("Ingrese el título del libro:");
                    String tituloLibro = scanner.nextLine();
                    Libro libroSeleccionado = null;
                    for (Libro libro : libros) {
                        if (libro.getTitulo().equals(tituloLibro)) {
                            libroSeleccionado = libro;
                            break;
                        }
                    }
                    if (libroSeleccionado == null) {
                        System.out.println("Libro no encontrado.");
                        break;
                    }
                    Venta nuevaVenta = new Venta(clienteSeleccionado, libroSeleccionado, new Date(), "Compra");
                    ventas.add(nuevaVenta);
                    System.out.println("Compra realizada exitosamente.");
                }
                case 4 -> {
                    System.out.println("Saliendo...");
                    return;
                }
                default -> System.out.println("Opción no válida.");
            }
        }
    }
}
