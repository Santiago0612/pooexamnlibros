package Logica;

import Logica.Libro;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    private String nombre;
    private String apellido;
    private String direccion;
    private List<Libro> librosCompradosOPrestados;

    public Cliente(String nombre, String apellido, String direccion) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.librosCompradosOPrestados = new ArrayList<>();
    }

    public void comprarLibro(Libro libro) {
        this.librosCompradosOPrestados.add(libro);
        libro.setNumCopiasDisponibles(libro.getNumCopiasDisponibles() - 1);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void devolverLibro(Libro libro) {
        this.librosCompradosOPrestados.remove(libro);
    }



    public List<Libro> getLibrosCompradosOPrestados() {
        return librosCompradosOPrestados;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", direccion='" + direccion + '\'' +
                ", librosCompradosOPrestados=" + librosCompradosOPrestados +
                '}';
    }
}
