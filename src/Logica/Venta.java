package Logica;

import java.util.Date;

public class Venta {
    private Cliente cliente;
    private Libro libro;
    private Date fecha;
    private String tipoTransaccion;

    public Venta(Cliente cliente, Libro libro, Date fecha, String tipoTransaccion) {
        this.cliente = cliente;
        this.libro = libro;
        this.fecha = fecha;
        this.tipoTransaccion = tipoTransaccion;
        cliente.comprarLibro(libro);
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    @Override
    public String toString() {
        return "Venta{" +
                "cliente=" + cliente.toString()+
                ", libro=" + libro.toString() +
                ", fecha=" + fecha +
                ", tipoTransaccion='" + tipoTransaccion + '\'' +
                '}';
    }
}

