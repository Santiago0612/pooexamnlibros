package Logica;

public class Libro {
    private String titulo;
    private String autor;
    private int año;
    private int numCopiasDisponibles;


    public Libro(String titulo, String autor, int año, int numCopiasDisponibles, Boolean esCompra) {
        this.titulo = titulo;
        this.autor = autor;
        this.año = año;
        this.numCopiasDisponibles = numCopiasDisponibles;

    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public int getNumCopiasDisponibles() {
        return numCopiasDisponibles;
    }

    public void setNumCopiasDisponibles(int numCopiasDisponibles) {
        this.numCopiasDisponibles = numCopiasDisponibles;
    }

    @Override
    public String toString() {
        return "Libro{" +
                "titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
                ", año='" + año + '\'' +
                ", numCopiasDisponibles='" + numCopiasDisponibles + '\'' +
                '}';
    }
}
